# Lazy Linux Setup

The lazy linux setup (lls) is suppossed to help you setting up your linux environment. You only need to fetch and run the payload script. Then you can select in a question/answer manner what software to install.

> Currently only tested with **Debian (Stretch/Buster)**

## Usage

*prerequisites:*
* *`curl` or `wget`*
* *internet connection*

You just need to fetch and run the payload script. To do this use the commands below:

**via wget**
```bash
    sudo sh -c "$(wget -O- https://gitlab.com/0xcccccccc/lazy_linux_setup/-/raw/master/payload.sh)"
```

**via curl**
```bash
    sudo sh -c "$(curl -fsSL https://gitlab.com/0xcccccccc/lazy_linux_setup/-/raw/master/payload.sh)"
```
  
The payload script will install `git` and fetch this repository. After that the *main script* (`lls.sh`) is executed und you will be prompted to select the components for installation.
  
*e.g.*  
![](./docs/devtools.png)
![](./docs/ovt.png)
![](./docs/vscode.png)

## Contribution
*ToDo*

## Source Structure
*ToDo*

## Testing
*ToDo*

## CI/CD
*ToDo*

## known issues
