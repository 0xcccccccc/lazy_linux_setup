#!/bin/bash

export LLS_TEST_SKIP_INSTALL=1

lls_plugin_show_name ()
{
    echo "Docker CE"
}

lls_plugin_show_description ()
{
    echo "Install Docker-CE"
}

lls_plugin_install ()
{
    set -e
    
    curl -fsSL https://get.docker.com -o ${LLS_PLUGIN_TMP_DIR}/get-docker.sh
    
    sh ${LLS_PLUGIN_TMP_DIR}/get-docker.sh

    /sbin/usermod -aG docker $SUDO_USER
}

source "./plugin/plugin.sh"