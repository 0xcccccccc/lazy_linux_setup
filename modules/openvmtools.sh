#!/bin/bash

lls_plugin_show_name ()
{
    echo "Open VM Tools"
}

lls_plugin_show_description ()
{
    echo "open-vm-tools is a set of services and modules that enable several features in VMware products for better management of,"
    echo "and seamless user interactions with, guests."
    echo "It includes kernel modules for enhancing the performance of virtual machines"
    echo "running Linux or other VMware supported Unix like guest operating systems."
}

lls_plugin_install ()
{
    apt-get install -y open-vm-tools
    apt-get install -y open-vm-tools-desktop
}

source "./plugin/plugin.sh"