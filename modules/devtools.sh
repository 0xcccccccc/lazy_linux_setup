#!/bin/bash

lls_plugin_show_name ()
{
    echo "Useful tools collection"
}

lls_plugin_show_description ()
{
    echo "Set of some useful tools:"
    echo "  - shellcheck"
    echo "  - jq"
    echo "  - tree"
    echo "  - tig"
    echo "  - sshpass"
}

lls_plugin_install ()
{
    apt-get -y install \
        shellcheck \
        jq \
        tree \
        tig \
        sshpass \
        software-properties-common
}

source "./plugin/plugin.sh"