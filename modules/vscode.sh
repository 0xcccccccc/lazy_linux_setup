#!/bin/bash

lls_plugin_show_name ()
{
    echo "VS Code"
}

lls_plugin_show_description ()
{
    echo "Visual Studio Code is a source-code editor developed by Microsoft for Windows, Linux and macOS."
}

lls_plugin_install ()
{
    which "code" &>/dev/null
    if [ $? -eq "1" ]; then
        curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
        install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/sudo install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/
        sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
        apt-get install -y apt-transport-https
        apt-get update
        apt-get install -y code
    else
        lls_plugin_log_info "code seems to be already installed"
    fi

}

source "./plugin/plugin.sh"