#!/bin/bash

lls_plugin_show_name ()
{
    echo "GO 1.14.2"
}

lls_plugin_show_description ()
{
    echo "Installs go in version 1.14.2"
}

lls_plugin_install ()
{
    GOVERSION="go1.14.2"
    GOTARNAME="${GOVERSION}.linux-amd64.tar.gz"

    command -v "go" &>/dev/null
    if [ $? -eq "1" ]; then
        curl https://dl.google.com/go/${GOTARNAME} -o "${LLS_PLUGIN_TMP_DIR}/${GOTARNAME}"
        tar xvf "${LLS_PLUGIN_TMP_DIR}/${GOTARNAME}" --directory "${LLS_PLUGIN_TMP_DIR}"
        chmod -R 777 "${LLS_PLUGIN_TMP_DIR}/go"
        mv "${LLS_PLUGIN_TMP_DIR}/go" "/usr/local/${GOVERSION}"
        echo "export PATH=$PATH:/usr/local/${GOVERSION}/bin" > "/etc/profile.d/go.sh"
    else
        lls_plugin_log_info "code seems to be already installed"
    fi

}

source "./plugin/plugin.sh"