#!/bin/bash

# set to 1 to skip the generated 'install test' during cd/cd
export LLS_TEST_SKIP_INSTALL=0

lls_plugin_show_name ()
{
    echo "Sample"
}

lls_plugin_show_description ()
{
    echo "Performs sample installation steps"
}

lls_plugin_install ()
{
    lls_plugin_log_info "starting sample install..."
    lls_plugin_log_warn "logging sample warning"
    lls_plugin_log_error "logging sample error"
    sleep 2
    lls_plugin_log_info "finished sample install"
}

source "./plugin/plugin.sh"