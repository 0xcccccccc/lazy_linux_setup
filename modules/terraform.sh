#!/bin/bash

#get latest version
LATEST_TERRAFORM_VERSION=$(curl -s https://releases.hashicorp.com/terraform/ | grep -o -P '(?<=\>terraform_)(.+)(?=\<)' | grep -Pxo '\d\.\d+\.\d+' | sort --version-sort -r | head -n 1)
    
lls_plugin_show_name ()
{
    echo "Terraform ${LATEST_TERRAFORM_VERSION}"
}

lls_plugin_show_description ()
{
    echo "Install latest Terraform"
}

lls_plugin_install ()
{
    set -e

    #build URL
    TERRAFORM_ZIP_NAME="terraform_${LATEST_TERRAFORM_VERSION}_linux_amd64.zip"
    TERRAFORM_DL_URL="https://releases.hashicorp.com/terraform/${LATEST_TERRAFORM_VERSION}"
    
    lls_plugin_log_info "Downloading Terraform version ${LATEST_TERRAFORM_VERSION}"
    lls_plugin_log_info "${TERRAFORM_DL_URL}/${TERRAFORM_ZIP_NAME}"

    rm -drf "${LLS_PLUGIN_TMP_DIR:?}/${TERRAFORM_ZIP_NAME}"
    rm -drf "${LLS_PLUGIN_TMP_DIR}/terraform"

    curl -o "${LLS_PLUGIN_TMP_DIR}/${TERRAFORM_ZIP_NAME}" -L "${TERRAFORM_DL_URL}/${TERRAFORM_ZIP_NAME}"

    unzip "${LLS_PLUGIN_TMP_DIR}/${TERRAFORM_ZIP_NAME}" -d "${LLS_PLUGIN_TMP_DIR}" 
    mv "${LLS_PLUGIN_TMP_DIR}/terraform" "$LLS_PLUGIN_USR_BIN_DIR"
    terraform -v
}

source "./plugin/plugin.sh"
