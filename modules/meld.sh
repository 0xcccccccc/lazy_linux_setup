#!/bin/bash

lls_plugin_show_name ()
{
    echo "Meld"
}

lls_plugin_show_description ()
{
    echo "Meld is a graphical diff viewer and merge application for the GNOME desktop."
    echo "It supports 2 and 3-file diffs, recursive directory diffs, diffing of directories"
    echo "under version control:" 
    echo "  - Bazaar"
    echo "  - Codeville"
    echo "  - CVS"
    echo "  - Darcs"
    echo "  - Fossil"
    echo "  - SCM"
    echo "  - Git"
    echo "  - Mercurial"
    echo "  - Monotone"
    echo "  - Subversion"
    echo ""
    echo "(as well as the ability to manually and automatically merge file differences.)"
}

lls_plugin_install ()
{
    apt-get install -y meld
}

source "./plugin/plugin.sh"