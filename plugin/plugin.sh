#!/bin/bash

#------------------------------------------------------------
#           
#               THE FOLLOWING METHODS HAVE TO
#               BE IMPLEMENTED BY THE PLUGINS
#
#       - lls_plugin_show_description
#       - lls_plugin_show_name
#       - lls_plugin_install
#
#------------------------------------------------------------

source "./lls/vars.sh"

source "./lls/log.sh"

if [ "$1" == "$LLS_PLUGIN_FLAG_DESCRIPTION" ]
then
    lls_plugin_show_description
    exit 0
fi

if [ "$1" == "$LLS_PLUGIN_FLAG_NAME" ]
then
    lls_plugin_show_name
    exit 0
fi

if [ "$1" == "$LLS_PLUGIN_FLAG_INSTALL" ]
then
    lls_plugin_install
    exit 0
fi
