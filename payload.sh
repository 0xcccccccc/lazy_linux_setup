#!/bin/bash

# check root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 1
fi


LLS_CLONE_DIR="./lls"
LLS_ENTRY_SCRIPT="./lls/lls.sh"
LLS_BRANCH="master"

PWD="$(pwd)"

if [ ! -f "$LLS_ENTRY_SCRIPT" ]
then

    #check for git
    
    if command -v git > /dev/null 2>&1
    then
        echo "[LLS-PAYLOAD][INFO] Found git"
    else
        echo "[LLS-PAYLOAD][INFO] Seems git is not installed."
        apt-get update        
        if apt-get install -y git
            "[LLS-PAYLOAD][INFO] installed git successfully"
        then
            echo "[LLS-PAYLOAD][ERROR] Failed to install git"
            echo "[LLS-PAYLOAD][ERROR] Exit (1)"
            exit 2
        fi        
    fi

    # clone the repo for all scriptds
    git clone --depth 1 -b ${LLS_BRANCH} https://gitlab.com/0xcccccccc/lazy_linux_setup ${LLS_CLONE_DIR}

    cd "${LLS_CLONE_DIR}" || exit 3
fi

echo "[LLS-PAYLOAD][INFO] Calling lls entry script '${LLS_ENTRY_SCRIPT}'"
${LLS_ENTRY_SCRIPT}

cd "$PWD" || exit 4

echo "[LLS-PAYLOAD][INFO] Exit 0"

exit 0