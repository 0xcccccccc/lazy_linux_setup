#!/bin/bash

source "./lls/log.sh"
source "./test/testvars.sh"

NUM_ERROR=0
NUM_INFO=0
NUM_WARN=0
NUM_STYLE=0

FILES_ERROR=()
FILES_WARN=()
FILES_INFO=()
FILES_STYLE=()

if [ ${LLS_TEST_SHELLCHECK_WARNING_AS_ERROR} -ne 0 ]
then
    lls_plugin_log_info " SET WARNING AS ERROR!!!"
fi


if ! command -v shellcheck > /dev/null 2>&1
then
    lls_plugin_log_error "Seems shellcheck is not installed. Please install shellcheck and retry!"
    exit 
fi

# get all *.sh files
LINT_FILES=$(find . -type f -name \*.sh)

check_error () 
{
    if [ "$1" -gt 0 ]
    then
        NUM_ERROR=$((NUM_ERROR + $1))
        FILES_ERROR+=("$2($1)")
    fi
}

check_warning () 
{
    if [ "$1" -gt 0 ]
    then
        NUM_WARN=$((NUM_WARN + $1))
        FILES_WARN+=("$2($1)")
    fi
} 

check_info () 
{
    if [ "$1" -gt 0 ]
    then
        NUM_INFO=$((NUM_INFO + $1))
        FILES_INFO+=("$2($1)")
    fi
} 

check_style () 
{
    if [ "$1" -gt 0 ]
    then
        NUM_STYLE=$((NUM_STYLE + $1))
        FILES_STYLE+=("$2($1)")
    fi
} 

for n in $LINT_FILES
do
    lls_plugin_log_info "---------------------"
    lls_plugin_log_info "$n"
    lls_plugin_log_info "---------------------"
    
    LINT_RESULT=$(shellcheck "$n" -f json | jq .)
    
    lls_plugin_log_info "$LINT_RESULT"
    
    check_error "$(echo "$LINT_RESULT" | grep -c '"level": "error"')" "$n"
    check_warning "$(echo "$LINT_RESULT" | grep -c '"level": "warning"')" "$n"
    check_info "$(echo "$LINT_RESULT" | grep -c '"level": "info"')" "$n"
    check_style "$(echo "$LINT_RESULT" | grep -c '"level": "style"')" "$n"
done

if [ "${NUM_ERROR}" -gt 0 ]
then
    lls_plugin_log_error ""
    lls_plugin_log_error " FILES WITH ERRORS:"
    for file in "${FILES_ERROR[@]}"
    do
        lls_plugin_log_error "    - $file"
    done
fi

if [ "${NUM_WARN}" -gt 0 ]
then
    lls_plugin_log_warn ""
    lls_plugin_log_warn " FILES WITH WARNINGS:"
    for file in "${FILES_WARN[@]}"
    do
        lls_plugin_log_warn "    - $file"
    done
fi

if [ "${NUM_INFO}" -gt 0 ]
then
    lls_plugin_log_info ""
    lls_plugin_log_info " FILES WITH INFO"
    for file in "${FILES_INFO[@]}"
    do
        lls_plugin_log_info "    - $file"
    done
fi

if [ "${NUM_STYLE}" -gt 0 ]
then
    lls_plugin_log_info ""
    lls_plugin_log_info " FILES WITH STYLES:"
    for file in "${FILES_STYLE[@]}"
    do
        lls_plugin_log_info "    - $file"
    done
fi

lls_plugin_log_info ""
lls_plugin_log_info "----------------------------------"
lls_plugin_log_info "  Style  : ${NUM_STYLE}"
lls_plugin_log_info "  Info   : ${NUM_INFO}"
lls_plugin_log_info "  Warning: ${NUM_WARN}"
lls_plugin_log_info "  Error  : ${NUM_ERROR}"
lls_plugin_log_info "----------------------------------"

if [ ${NUM_ERROR} -gt 0 ]
then
    lls_plugin_log_info ""
    lls_plugin_log_error "Quality gate failed du to presence of 'Errors'"
    exit 1
fi

if [ ${LLS_TEST_SHELLCHECK_WARNING_AS_ERROR} -ne 0 ] && [ ${NUM_WARN} -gt 0 ]
then
    lls_plugin_log_info ""
    lls_plugin_log_error " Quality gate failed du to presence of 'Warnings'"
    lls_plugin_log_info " (you can disable 'warning as error' in './test/testvars.sh')"
    exit 1
fi

exit 0