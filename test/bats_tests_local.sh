#!/bin/bash

source ./test/testvars.sh
source ./lls/log.sh

lls_plugin_log_info "START RUNNING DOCKERIZED TESTS"

TEST_SINGLE_MODULE="$1"

#
# check docker 
#
if ! command -v docker > /dev/null 2>&1
then
    lls_plugin_log_error "--------------------------------------------"
    lls_plugin_log_error "Seems Docker isn't installed. Install Docker and try again!"
    lls_plugin_log_error "--------------------------------------------"
    exit 1
fi

#
# build test image
#
if ! sudo docker build -t "${LLS_TEST_DOCKER_IMAGE}" ./test/
then
    
    lls_plugin_log_error "--------------------------------------------"
    lls_plugin_log_error "Building test image failed. Please check output"
    lls_plugin_log_error "--------------------------------------------"
    exit 2
fi

#
# run test scripts inside test image container
#
if ! sudo docker run -v "$(pwd)/:/lazy_linux_setup" "${LLS_TEST_DOCKER_IMAGE}" "/lazy_linux_setup/test/bats_tests.sh" "${TEST_SINGLE_MODULE}"
then
    lls_plugin_log_error "--------------------------------------------"
    lls_plugin_log_error " One ore more tests failed. Check output!"
    lls_plugin_log_error "--------------------------------------------"
    exit 3
fi


lls_plugin_log_info "FINISHED RUNNING DOCKERIZED TESTS"
lls_plugin_log_info ""
lls_plugin_log_info " NO ISSUES FOUND"
lls_plugin_log_info "-------------------"
exit 0