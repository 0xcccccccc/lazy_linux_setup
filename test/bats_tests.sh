#!/bin/bash


source ./plugin/plugin.sh
source ./test/testvars.sh

lls_plugin_log_info " START RUNNING BATS TESTS"

TEST_SINGLE_MODULE="$1"

if [ -n "$TEST_SINGLE_MODULE" ]
then
    lls_plugin_log_info "single test detected ($1)"
fi

set -e

rm -drf "${LLS_PLUGIN_TMP_DIR}"
mkdir -p "${LLS_PLUGIN_TMP_DIR}"

#---------------------------------------------------------------
# BASIC TESTS
#---------------------------------------------------------------
lls_plugin_log_info " run basic tests"
#basic tests
for batsfile in ${LLS_TEST_BASIC_BATS}/*.bats
do
    lls_plugin_log_info "------>  $batsfile"
    bats "$batsfile"
done

#---------------------------------------------------------------
# GENERATED TESTS
#---------------------------------------------------------------
lls_plugin_log_info " run generated tests"
#generate basic module bats
rm -drf ${LLS_TEST_GENERATED_BATS}/*.bats

#generate test bats
for module in "${LLS_PLUGIN_DIR}"/*.sh
do
    sed "s@==NAME==@$module@g" ${LLS_TEST_GENERATED_BATS}/template > "${LLS_TEST_GENERATED_BATS}/$(basename $module).bats"
done

#run generated bats
if [ -z "$TEST_SINGLE_MODULE" ]
then
    for batsfile in ${LLS_TEST_GENERATED_BATS}/*.bats
    do
        lls_plugin_log_info "------>  $batsfile"
        bats "$batsfile"
    done
else
    BATSFILE="${LLS_TEST_GENERATED_BATS}/${TEST_SINGLE_MODULE}.sh.bats"
    if [ -f "${BATSFILE}" ]
    then
        bats "${BATSFILE}"
    else
        lls_plugin_log_warn "could not find ${BATSFILE}"
    fi
fi

#---------------------------------------------------------------
# SPECIFIC TESTS
#---------------------------------------------------------------
lls_plugin_log_info " run specific tests"
if [ -z "$TEST_SINGLE_MODULE" ]
then
    for batsfile in ${LLS_TEST_MODULE_BATS}/*.bats
    do
        lls_plugin_log_info "------>  $batsfile"
        bats "$batsfile"
    done
else
    BATSFILE="${LLS_TEST_MODULE_BATS}/${TEST_SINGLE_MODULE}.bats"
    if [ -f "${BATSFILE}" ]
    then
        bats "${BATSFILE}"
    else
        lls_plugin_log_warn "could not find ${BATSFILE}"
    fi
fi

lls_plugin_log_info " FINISHED RUNNING BATS TESTS"
