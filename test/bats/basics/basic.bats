#!/usr/bin/env bats


function setup() {

    export DEBIAN_FRONTEND=noninteractive 
    export DEBCONF_NONINTERACTIVE_SEEN=true

    debconf-set-selections "./test/helper/debconf/tzdata.conf"
    debconf-set-selections "./test/helper/debconf/keyboard.conf"
    
}

@test "./lls/install_basics.sh can be installed" { 
  
  run ./lls/install_basics.sh >&3
  [ "$status" -eq 0 ]

}

@test "unzip binary is available after calling ./lls/install_basics.sh" {
  run command -v unzip
  [ "$status" -eq 0 ]
}

@test "curl binary is available after calling ./lls/install_basics.sh" {
  run command -v curl
  [ "$status" -eq 0 ]
}