#!/usr/bin/env bats


    #echo "Set of some useful tools:"
    #echo "  - shellcheck"
    #echo "  - jq"
    #echo "  - tree"
    #echo "  - tig"
    #echo "  - sshpass"


@test 'shellcheck available' {
    run command -v shellcheck
    [ "$status" -eq 0 ]
}

@test 'jq available' {
    run command -v jq
    [ "$status" -eq 0 ]
}

@test 'tree available' {
    run command -v tree
    [ "$status" -eq 0 ]
}

@test 'tig available' {
    run command -v tig
    [ "$status" -eq 0 ]
}

@test 'sshpass available' {
    run command -v sshpass
    [ "$status" -eq 0 ]
}

