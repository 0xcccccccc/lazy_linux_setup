#!/usr/bin/env bats

TESTGOVERSION="go1.13.10"
GOBINPATH="/usr/local/${TESTGOVERSION}/bin/go"

@test "${TESTGOVERSION} binary available" {

    [ -f "$GOBINPATH" ]
}

@test "${TESTGOVERSION} env works" {

    run "$GOBINPATH" env
    [ "$status" -eq 0 ]

}

@test "${TESTGOVERSION} version" {
   
        run sh -c "$GOBINPATH version | grep ${TESTGOVERSION}"
    [ "$status" -eq 0 ]

}

@test "${TESTGOVERSION} /etc/profile.d/go.sh is available" {
    [ -f "/etc/profile.d/go.sh" ] 
}
