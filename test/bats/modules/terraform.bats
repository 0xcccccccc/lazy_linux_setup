#!/usr/bin/env bats



    @test 'terraform available' {
        run command -v terraform
        [ "$status" -eq 0 ]
    }
    
    