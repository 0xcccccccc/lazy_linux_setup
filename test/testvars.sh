#!/bin/bash

export LLS_TEST_SHELLCHECK_WARNING_AS_ERROR=1

export LLS_TEST_BASIC_BATS="./test/bats/basics"
export LLS_TEST_GENERATED_BATS="./test/bats/generated"
export LLS_TEST_MODULE_BATS="./test/bats/modules"
export LLS_TEST_DOCKER_IMAGE="lazy-linux-install:test"