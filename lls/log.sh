#!/bin/bash

source "./lls/vars.sh"

lls_plugin_log ()
{
    echo "${LLS_LOG_PREFIX}$*"
}

lls_plugin_log_info()
{
    lls_plugin_log "${LLS_LOG_INFO}$*"
}

lls_plugin_log_warn()
{
    lls_plugin_log "${LLS_LOG_WARN}$*"
}

lls_plugin_log_error()
{
    lls_plugin_log "${LLS_LOG_ERROR}$*"
}