#!/bin/bash

export LLS_PLUGIN_FLAG_DESCRIPTION="--description"
export LLS_PLUGIN_FLAG_INSTALL="--install"
export LLS_PLUGIN_FLAG_NAME="--name"

export LLS_PLUGIN_USR_BIN_DIR="/usr/bin"
export LLS_PLUGIN_TMP_DIR="./lls_tmp"
export LLS_PLUGIN_DIR="./modules"

export LLS_LOG_PREFIX="[LLS]"
export LLS_LOG_INFO="[INFO]"
export LLS_LOG_WARN="[WARN]"
export LLS_LOG_ERROR="[ERROR]"