#!/bin/bash

source ./plugin/plugin.sh

lls_plugin_log_info "installing basics"

#disable cdrom stuff in sources.list 
sed -i "/^deb cdrom:/s/^/#/" /etc/apt/sources.list

apt-get update

apt-get -y install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common \
     unzip
