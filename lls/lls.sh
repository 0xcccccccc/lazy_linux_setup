#!/bin/bash

source "./plugin/plugin.sh"

#-----------------------------------------------------------------
# GLOBAL VARS

LLS_MODULES_TO_INSTALL[]=

#-----------------------------------------------------------------
# FUNCTIONS

lls_prepare ()
{
    mkdir -p "${LLS_PLUGIN_TMP_DIR}"
}

lls_cleanup ()
{
    rm -drf "${LLS_PLUGIN_TMP_DIR}"
}


lls_ask_module_install ()
{
    PLUGIN_COMP_NAME=$("$1" "${LLS_PLUGIN_FLAG_NAME}")
    PLUGIN_COMP_DESC=$("$1" "${LLS_PLUGIN_FLAG_DESCRIPTION}")

    clear

    echo "'${PLUGIN_COMP_NAME}'"
    echo "-----"
    echo "${PLUGIN_COMP_DESC}"
    echo "--------------------------------------------------"
    echo ""

    while true; do
        read -p "Install '${PLUGIN_COMP_NAME}'? [y]es/[n]o " yn
        case $yn in
            [Yy]* ) LLS_MODULES_TO_INSTALL=("${LLS_MODULES_TO_INSTALL[*]}" "$1"); return 0;;
            [Nn]* ) return 1;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}


lls_fill_list_of_modules_to_install ()
{
    for entry in "${LLS_PLUGIN_DIR}"/*.sh
    do
        lls_ask_module_install "$entry"
    done
}

lls_install_selected_modules ()
{
    for plugin in ${LLS_MODULES_TO_INSTALL[*]}
    do
        lls_plugin_log_info ""
        lls_plugin_log_info ""

        lls_plugin_log_info "Start Install '$(${plugin} ${LLS_PLUGIN_FLAG_NAME})' --> ${plugin}"
        ${plugin} ${LLS_PLUGIN_FLAG_INSTALL}

        lls_plugin_log_info ""
        lls_plugin_log_info ""
    done
}

#-----------------------------------------------------------------
# SCRIPT


./lls/install_basics.sh

lls_prepare
lls_fill_list_of_modules_to_install
lls_install_selected_modules
lls_cleanup

